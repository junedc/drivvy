package com.aut.drivvy.user;


import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import android.content.Intent;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.aut.drivvy.R;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.ItemTouchHelper;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import java.util.List;

public class UserListActivity extends AppCompatActivity {

    public static final int ADD_USER_REQUEST = 1;
    public static final int EDIT_USER_REQUEST = 2;

    private UserViewModel viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_container);

        FloatingActionButton buttonAddUser = findViewById(R.id.button_add_user);
        buttonAddUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getBaseContext(), UserActivity.class);
                startActivityForResult(intent, ADD_USER_REQUEST);
            }
        });

        RecyclerView recyclerView = findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setHasFixedSize(true);

        final UserAdapter adapter = new UserAdapter();
        recyclerView.setAdapter(adapter);

        viewModel = ViewModelProviders.of(this).get(UserViewModel.class);
        viewModel.getAllUsers().observe(this, new Observer<List<User>>() {
            @Override
            public void onChanged(@Nullable List<User> users) {

                adapter.submitList(users);

            }
        });

        new ItemTouchHelper(new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {

            @Override
            public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder viewHolder1) {
                return false;
            }

            @Override
            public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
                viewModel.delete(adapter.getUserAt(viewHolder.getAdapterPosition()));

                Toast.makeText(getBaseContext(), R.string.user_deleted, Toast.LENGTH_SHORT).show();
            }
        }).attachToRecyclerView(recyclerView);

        adapter.setOnItemClickListener(new UserAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(User user) {
                Intent intent = new Intent(getBaseContext(), UserActivity.class);

                intent.putExtra(UserActivity.EXTRA_ID, user.getId());
                intent.putExtra(UserActivity.EXTRA_DRIVER, user.getDriver());
                intent.putExtra(UserActivity.EXTRA_EMAIL, user.getEmail());
                intent.putExtra(UserActivity.EXTRA_PASSWORD, user.getPassword());

                startActivityForResult(intent, EDIT_USER_REQUEST);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == ADD_USER_REQUEST && resultCode == RESULT_OK) {
            String driver = data.getStringExtra(UserActivity.EXTRA_DRIVER);
            String email = data.getStringExtra(UserActivity.EXTRA_EMAIL);
            String password = data.getStringExtra(UserActivity.EXTRA_PASSWORD);
            User user = new User(driver, email, password);

            viewModel.insert(user);
            Toast.makeText(this, R.string.user_created, Toast.LENGTH_SHORT).show();
        } else if (requestCode == EDIT_USER_REQUEST && resultCode == RESULT_OK) {
            int id = data.getIntExtra(UserActivity.EXTRA_ID, -1);

            if (id == -1) {
                Toast.makeText(this, R.string.not_saved, Toast.LENGTH_SHORT).show();
                return;
            }

            String driver = data.getStringExtra(UserActivity.EXTRA_DRIVER);
            String email = data.getStringExtra(UserActivity.EXTRA_EMAIL);
            String password = data.getStringExtra(UserActivity.EXTRA_PASSWORD);

            User user = new User(driver, email, password);
            user.setId(id);
            viewModel.update(user);

            Toast.makeText(this, R.string.updated_successfully, Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, R.string.not_saved, Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.delete_users_menu, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case R.id.delete_all_users:
                viewModel.deleteAllUsers();
                Toast.makeText(this, R.string.user_all_deleted, Toast.LENGTH_SHORT).show();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}