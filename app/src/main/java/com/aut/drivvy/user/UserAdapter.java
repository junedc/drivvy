package com.aut.drivvy.user;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.ListAdapter;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.aut.drivvy.R;

public class UserAdapter extends ListAdapter<User, UserAdapter.UserHolder> {

    private OnItemClickListener listener;

    public UserAdapter() {
        super(DIFF_CALLBACK);
    }

    private static final DiffUtil.ItemCallback<User> DIFF_CALLBACK =
            new DiffUtil.ItemCallback<User>() {

                @Override
                public boolean areItemsTheSame(@NonNull User oldItem, @NonNull User newItem) {
                    return oldItem.getId() == newItem.getId();
                }

                @Override
                public boolean areContentsTheSame(@NonNull User oldItem, @NonNull User newItem) {
                    return oldItem.getDriver().equals(newItem.getDriver()) &&
                            oldItem.getEmail().equals(newItem.getEmail()) ;
                }
            };

    @NonNull
    @Override
    public UserHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.activity_user_list, viewGroup, false);
        return new UserHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull UserHolder viewHolder, int position) {
        User currentUser = getItem(position);
        viewHolder.textViewDriver.setText(currentUser.getDriver());
//        viewHolder.textViewEmail.setText(currentUser.getEmail());
//        viewHolder.textViewPassword.setText(currentUser.getPassword());
//        viewHolder.imageDriver.setImageResource();
    }

    public User getUserAt(int position) {
        return getItem(position);
    }

    class UserHolder extends RecyclerView.ViewHolder {
        private TextView textViewDriver;
        private TextView textViewEmail;
        private TextView textViewPassword;
        private ImageView imageDriver;

        private UserHolder(View itemView) {
            super(itemView);
            textViewDriver = itemView.findViewById(R.id.text_driver);

//            textViewEmail = itemView.findViewById(R.id.edit_text_email);
//            textViewPassword = itemView.findViewById(R.id.edit_text_password);
            imageDriver = itemView.findViewById(R.id.imageView);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition();

                    if (listener != null && position != RecyclerView.NO_POSITION) {
                        listener.onItemClick(getItem(position));
                    }
                }
            });
        }
    }

    public interface OnItemClickListener {
        void onItemClick(User user);
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        this.listener = listener;
    }
}
