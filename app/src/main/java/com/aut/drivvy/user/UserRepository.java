package com.aut.drivvy.user;

import android.app.Application;
import androidx.lifecycle.LiveData;
import android.os.AsyncTask;

import java.util.List;

public class UserRepository {

    private UserDao userDAO;
    private LiveData<List<User>> allUsers;

    public UserRepository(Application application) {
        UserDatabase db = UserDatabase.getInstance(application);
        userDAO = db.userDAO();
        allUsers = userDAO.getAllUsers();
    }

    public void insert(User user) {
        new InsertUserAsyncTask(userDAO).execute(user);
    }

    public void update(User user) {
        new UpdateUserAsyncTask(userDAO).execute(user);
    }

    public void delete(User user) {
        new DeleteUserAsyncTask(userDAO).execute(user);
    }

    public void deleteAllUsers() {
        new DeleteAllUserAsyncTask(userDAO).execute();
    }

    public LiveData<List<User>> getAllUsers() {
        return allUsers;
    }

    public static class InsertUserAsyncTask extends AsyncTask<User, Void, Void> {

        private UserDao userDAO;
        private InsertUserAsyncTask(UserDao userDAO) {
            this.userDAO = userDAO;
        }

        @Override
        protected Void doInBackground(User... users) {

            userDAO.insert(users[0]);
            return null;
        }
    }

    public static class UpdateUserAsyncTask extends AsyncTask<User, Void, Void> {

        private UserDao userDAO;
        private UpdateUserAsyncTask(UserDao userDAO) {
            this.userDAO = userDAO;
        }

        @Override
        protected Void doInBackground(User... users) {

            userDAO.update(users[0]);
            return null;
        }
    }

    public static class DeleteUserAsyncTask extends AsyncTask<User, Void, Void> {

        private UserDao userDao;
        private DeleteUserAsyncTask(UserDao userDao) {
            this.userDao = userDao;
        }

        @Override
        protected Void doInBackground(User... users) {

            userDao.delete(users[0]);
            return null;
        }
    }

    public static class DeleteAllUserAsyncTask extends AsyncTask<User, Void, Void> {

        private UserDao userDAO;
        private DeleteAllUserAsyncTask(UserDao userDAO) {
            this.userDAO = userDAO;
        }

        @Override
        protected Void doInBackground(User... users) {

            userDAO.deleteAllUsers();
            return null;
        }
    }
}
