package com.aut.drivvy.user;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;

import com.aut.drivvy.R;

public class UserActivity extends AppCompatActivity {

    public static final String EXTRA_ID = "com.aut.drivvy.EXTRA_ID";
    public static final String EXTRA_DRIVER = "com.aut.drivvy.EXTRA_DRIVER";
    public static final String EXTRA_EMAIL = "com.aut.drivvy.EXTRA_EMAIL";
    public static final String EXTRA_PASSWORD = "com.aut.drivvy.EXTRA_PASSWORD";

    private EditText editTextDriver;
    private EditText editTextEmail;
    private EditText editTextPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user);

        editTextDriver = findViewById(R.id.edit_text_driver);
        editTextEmail = findViewById(R.id.edit_text_email);
        editTextPassword = findViewById(R.id.edit_text_password);

        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_close);

        Intent intent = getIntent();
        if (intent.hasExtra(EXTRA_ID)) {
            setTitle(R.string.user_title_edit);
            editTextDriver.setText(intent.getStringExtra(EXTRA_DRIVER));
            editTextEmail.setText(intent.getStringExtra(EXTRA_EMAIL));
            editTextPassword.setText(intent.getStringExtra(EXTRA_PASSWORD));
        } else {
            setTitle(R.string.user_title_add);
        }
    }

    private void saveUser() {
        String driver = editTextDriver.getText().toString();
        String email = editTextEmail.getText().toString();
        String password = editTextPassword.getText().toString();

        if (driver.trim().isEmpty() || email.trim().isEmpty()) {
            Toast.makeText(this, R.string.user_validation_name, Toast.LENGTH_SHORT).show();
            return;
        }

        Intent data = new Intent();
        data.putExtra(EXTRA_DRIVER, driver);
        data.putExtra(EXTRA_EMAIL, email);
        data.putExtra(EXTRA_PASSWORD, password);

        int id = getIntent().getIntExtra(EXTRA_ID, -1);
        if (id != -1) {
            data.putExtra(EXTRA_ID, id);
        }

        setResult(RESULT_OK, data);
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.add_user_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.save_user:
                saveUser();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
